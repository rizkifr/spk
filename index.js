import express from "express";
import cors from 'cors'
import Router from "./routes/routes.js";
import env from 'dotenv'
env.config()

const app = express();

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));
app.use(cors());
app.use(Router);
app.use(express.static('public'));

app.listen(process.env.PORT, () => console.log(`Server Running at http://localhost:${process.env.PORT}`));