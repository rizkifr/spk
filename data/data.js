export const kultur_tanah = [
    [1, 3, 2, 1 / 5, 1 / 3, 3, 1 / 5, 7],
    [1 / 3, 1, 1 / 5, 5, 5, 1 / 5, 7, 1 / 7],
    [1 / 2, 5, 1, 3, 1 / 3, 5, 5, 7],
    [5, 1 / 5, 1 / 3, 1, 3, 5, 1 / 3, 1 / 3],
    [3, 1 / 5, 3, 1 / 3, 1, 1 / 3, 3, 1 / 5],
    [1 / 3, 5, 1 / 5, 1 / 5, 3, 1, 1 / 5, 1 / 3],
    [5, 1 / 5, 1 / 5, 3, 1 / 3, 5, 1, 3],
    [1 / 7, 7, 1 / 7, 3, 1 / 5, 3, 1 / 3, 1],

]

export const jumlah_kamar = [
    [1, 1 / 3, 2, 1 / 3, 4, 1 / 5, 6, 1 / 3],
    [3, 1, 1 / 3, 2, 1 / 3, 5, 1 / 5, 7],
    [1 / 2, 3, 1, 1 / 3, 3, 1 / 5, 5, 1 / 7],
    [3, 1 / 3, 3, 1, 1 / 3, 3, 1 / 5, 3],
    [1 / 4, 3, 1 / 4, 3, 1, 1 / 3, 3, 1 / 5],
    [5, 1 / 5, 5, 1 / 3, 3, 1, 1 / 3, 5],
    [1 / 6, 5, 1 / 5, 5, 1 / 3, 3, 1, 1 / 7],
    [3, 1 / 7, 7, 1 / 3, 5, 1 / 5, 7, 1],

]

export const luas_parkiran = [
    [1, 3, 1 / 2, 5, 1 / 7, 1 / 3, 2, 1 / 5],
    [1 / 3, 1, 3, 1 / 5, 5, 7, 1 / 3, 3],
    [2, 1 / 3, 1, 3, 1 / 5, 5, 5, 1 / 7],
    [1 / 5, 5, 1 / 3, 1, 3, 1 / 3, 1 / 5, 3],
    [7, 1 / 5, 5, 1 / 3, 1, 3, 1 / 3, 1 / 3],
    [3, 1 / 7, 1 / 5, 3, 1 / 3, 1, 3, 5],
    [1 / 2, 3, 1 / 5, 5, 3, 1 / 3, 1, 1 / 3],
    [5, 1 / 3, 7, 1 / 3, 3, 1 / 5, 3, 1],

]

export const spek_bangunan = [
    [1, 1 / 3, 5, 1 / 2, 6, 1 / 3, 4, 1 / 3],
    [3, 1, 1 / 5, 3, 1 / 3, 5, 1 / 3, 5],
    [1 / 5, 5, 1, 1 / 5, 3, 1 / 5, 7, 1 / 3],
    [2, 1 / 3, 5, 1, 1 / 7, 5, 1 / 3, 7],
    [1 / 6, 3, 1 / 3, 7, 1, 1 / 4, 3, 1 / 5],
    [3, 1 / 5, 5, 1 / 5, 4, 1, 1 / 5, 5],
    [1 / 6, 3, 1 / 7, 3, 1 / 3, 5, 1, 3],
    [3, 1 / 5, 3, 1 / 7, 5, 1 / 5, 1 / 3, 1],

]

export const design = [
    {
        name: 'D1',
        type: '60',
        desc: 'Type 60 1 Laintai',
        images: ['1.png', '2.png', '3.png', '4.png', '5.png'],
        interior_and_exterior: {
            luas_tanah: "60 m2",
            lebar_muka: "5 m2",
            panjang_bangunan: "12 m2",
            jumlah_kamar_tidur: "2",
            jumlah_kamar_mandi: "1",
            luas_carport: "4,5 x 2,5",
            harga: "Rp. 400.000.000 – Rp. 450.000.000"
        },
        specification_recomendation: [
            "Atap Baja Ringan",
            "Bata Hebel",
            "Lantai Granit 60 x 60",
            "Cakar Ayam 14",
            "Kaca 8 mm",
            "Besi 10 inchi",
            "Kanopi"
        ]
    },
    {
        name: 'D2',
        type: '60',
        desc: 'Type 60 2 Laintai',
        images: ['1.png', '2.png', '3.png', '4.png', '5.png', '6.png', '7.png', '8.png'],
        interior_and_exterior: {
            luas_tanah: "60 m2",
            lebar_muka: "5 m2",
            panjang_bangunan: "12 m2",
            jumlah_kamar_tidur: "2",
            jumlah_kamar_mandi: "1",
            luas_carport: "4,5 x 2,5",
            harga: "Rp. 450.000.000 – Rp. 500.000.000"
        },
        specification_recomendation: [
            "Atap Baja Ringan",
            "Bata Hebel",
            "Lantai Granit 60 x 60",
            "Cakar Ayam 18",
            "Kaca 8 mm",
            "Besi 10 inchi",
            "Kanopi"
        ]
    },
    {
        name: 'D3',
        type: '70',
        desc: 'Type 70 1 Laintai',
        images: ['1.png', '2.png', '3.png', '4.png', '5.png'],
        interior_and_exterior: {
            luas_tanah: "70 m2",
            lebar_muka: "7 m2",
            panjang_bangunan: "10 m2",
            jumlah_kamar_tidur: "3",
            jumlah_kamar_mandi: "2",
            luas_carport: "4,5 x 2,5",
            harga: "Rp. 550.000.000 – Rp. 600.000.000"
        },
        specification_recomendation: [
            "Atap Baja Ringan",
            "Bata Hebel",
            "Lantai Granit 60 x 60",
            "Cakar Ayam 18",
            "Kaca 8 mm",
            "Besi 10 inchi",
            "Kanopi"
        ]
    },
    {
        name: 'D4',
        type: '70',
        desc: 'Type 70 2 Laintai',
        images: ['1.png', '2.png', '3.png', '4.png', '5.png', '6.png'],
        interior_and_exterior: {
            luas_tanah: "70 m2",
            lebar_muka: "7 m2",
            panjang_bangunan: "10 m2",
            jumlah_kamar_tidur: "3",
            jumlah_kamar_mandi: "2",
            luas_carport: "4,5 x 2,5",
            harga: "Rp. 650.000.000 – Rp. 700.000.000"
        },
        specification_recomendation: [
            "Atap Baja Ringan",
            "Bata Hebel",
            "Lantai Granit 60 x 60",
            "Cakar Ayam 20",
            "Kaca 8 mm",
            "Besi 10 inchi",
            "Kanopi"
        ]
    },
    {
        name: 'D5',
        type: '100',
        desc: 'Type 100 1 Laintai',
        images: ['1.png', '2.png', '3.png', '4.png', '5.png'],
        interior_and_exterior: {
            luas_tanah: "100 m2",
            lebar_muka: "10 m2",
            panjang_bangunan: "10 m2",
            jumlah_kamar_tidur: "4",
            jumlah_kamar_mandi: "3",
            luas_carport: "5 x 2,5",
            harga: "Rp. 800.000.000 – Rp. 850.000.000"
        },
        specification_recomendation: [
            "Atap Baja Ringan",
            "Bata Hebel",
            "Lantai Granit 60 x 60",
            "Cakar Ayam 22",
            "Kaca 10 mm",
            "Besi 12 inchi",
            "Kanopi"
        ]
    },
    {
        name: 'D6',
        type: '100',
        desc: 'Type 100 2 Laintai',
        images: ['1.png', '2.png', '3.png', '4.png', '5.png', '6.png', '7.png', '8.png'],
        interior_and_exterior: {
            luas_tanah: "100 m2",
            lebar_muka: "10 m2",
            panjang_bangunan: "10 m2",
            jumlah_kamar_tidur: "4",
            jumlah_kamar_mandi: "3",
            luas_carport: "5 x 2,5",
            harga: "Rp. 850.000.000 – Rp. 900.000.000"
        },
        specification_recomendation: [
            "Atap Baja Ringan",
            "Bata Hebel",
            "Lantai Granit 60 x 60",
            "Cakar Ayam 24",
            "Kaca 10 mm",
            "Besi 12 inchi",
            "Kanopi"
        ]
    },
    {
        name: 'D7',
        type: '200',
        desc: 'Type 200 1 Laintai',
        images: ['1.png', '2.png', '3.png', '4.png', '5.png'],
        interior_and_exterior: {
            luas_tanah: "200 m2",
            lebar_muka: "10 m2",
            panjang_bangunan: "20 m2",
            jumlah_kamar_tidur: "5",
            jumlah_kamar_mandi: "4",
            luas_carport: "4,5 x 2,5",
            ruang_musola: "1",
            balkon: "2",
            harga: "Rp. 1000.000.000 – Rp. 1.050.000.000"
        },
        specification_recomendation: [
            "Atap Baja Ringan",
            "Bata Hebel",
            "Lantai Granit 60 x 60",
            "Cakar Ayam 28",
            "Kaca 10 mm",
            "Besi 12 inchi",
            "Kanopi"
        ]
    },
    {
        name: 'D8',
        type: '200',
        desc: 'Type 200 2 Laintai',
        images: ['1.png', '2.png', '3.png', '4.png', '5.png', '6.png'],
        interior_and_exterior: {
            luas_tanah: "200 m2",
            lebar_muka: "10 m2",
            panjang_bangunan: "20 m2",
            jumlah_kamar_tidur: "5",
            jumlah_kamar_mandi: "4",
            luas_carport: "4,5 x 2,5",
            ruang_musola: "1",
            balkon: "2",
            harga: "Rp. 1.050.000.000 – Rp. 1.100.000.000"
        },
        specification_recomendation: [
            "Atap Baja Ringan",
            "Bata Hebel",
            "Lantai Granit 60 x 60",
            "Cakar Ayam 32",
            "Kaca 10 mm",
            "Besi 12 inchi",
            "Kanopi"
        ]
    },
]